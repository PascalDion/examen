package codeActif;

public class ClasseChaine {

    // Codage sans aucun contr�le d'erreur
    public static String RetourneInitialesSansControle(String prenomNom)
    {
        String[] tab = prenomNom.split(" ");
        return String.format("%c.%c.", tab[0].charAt(0),tab[1].charAt(0));
    }

    // Codage avec contr�le d'erreur
    public static String RetourneInitiales(String prenomNom)
    {
        if (prenomNom == null || prenomNom.length() == 0)
            return "";

        String[] tab = prenomNom.split(" ");
        if( tab.length != 2)
            return "";

        return String.format("%c.%c.", tab[0].charAt(0),tab[1].charAt(0));
    }

}
