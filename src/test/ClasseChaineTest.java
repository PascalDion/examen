package test;

import codeActif.ClasseChaine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ClasseChaineTest {

	@Test
	void testCasNormal() {
		String initiales = ClasseChaine.RetourneInitiales("SMA Mbibi");
		// initiales doit contenir "S.M."
		assertTrue(initiales.equals("S.M."), "Attendu: S.M. Reu: " + initiales);
	}

	@Test
	void testParametreNull() {
		String initiales = ClasseChaine.RetourneInitiales(null);
		assertTrue(initiales.equals(""), "TestParametreNull : Chaine vide attendue mais non recue");
	}

	@Test
	void testChaineVide() {
		String initiales = ClasseChaine.RetourneInitiales("");
		assertTrue(initiales.equals(""), "TestChaineVide : Chaine vide attendue mais non recue");
	}

	@Test
	void testChainePartielle() {
		String initiales = ClasseChaine.RetourneInitiales("Andreas");
		assertTrue(initiales.equals(""), "TestChainePartielle : Chaine vide attendue mais non recue");
	}
}